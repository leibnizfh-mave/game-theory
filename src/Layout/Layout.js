import React from 'react';
import DozentMenueLeiste from "./DozentMenueLeiste";


// Einbinden der DozentMenüLeiste als übergeordnetes Layout
const Layout =({children}) => {
    return (
        <>
        <div>
            <DozentMenueLeiste></DozentMenueLeiste>
        </div>
            <main>{children}</main>
        </>


    );
}


export default Layout;
