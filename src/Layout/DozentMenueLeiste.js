import React from 'react';
import './MenueLeiste.css';



class DozentMenueLeiste extends React.Component {
    render() {
        return (
            <div class="page">
                <div className="icon_bar">

                    {/*Dropdown bei User Icon*/}
                    <div className="dropdown">
                        <button className="dropbtn"><i className="fa fa-user"></i></button>

                        <div className="dropdown-content">

                            <div className ="userMenu">
                                <a href="#">Kontoeinstellungen</a>
                            </div>

                            <div className ="userMenu">
                                <a href="#">Abmelden</a>
                            </div>


                        </div>
                    </div>
                    <div className="logo_gametheory">Game Theory</div>
                </div>
                <div class="content"></div>
                <div class="footer"></div>
            </div>



        );
    }

}

export default DozentMenueLeiste;
