class Round{
    //Constructor der Class Round
    constructor(Id, RoundName) {
        this.Id = Id;
        this.RoundName = RoundName;
    }

    //Getter Id
    get Id(){
        return this.Id;
    }
    //Setter Id
    set Id(id){
        this.Id(id);
    }

    //Getter RoundName
    get RoundName() {
        return this.RoundName;
    }

    //Setter RoundName
    set RoundName(roundname){
        this.RoundName(roundname);
    }
}