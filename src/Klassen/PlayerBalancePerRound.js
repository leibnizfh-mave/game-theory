class PlayerBalancePerRound{
    constructor(Game, Player, Round, Balance){
        this.Game = Game;
        this.Player = Player;
        this.Round = Round;
        this.Balance = Balance;
    }

    get Game(){
        return this.Game;
    }
    set Game(game){
        this.Game(game)
    }

    get Player(){
        return this.Player;
    }
    set Player(player){
        this.Player(player)
    }

    get Round(){
        return this.Round;
    }
    set Round(round){
        this.Round(round)
    }

    get Balance(){
        return this.Balance;
    }
    set Balance(balance){
        this.Balance(balance)
    }
}