class PlayerInGame{


    constructor(Id, Game, Player) {
        this.Id =Id;
        this.Game = Game;
        this.Player = Player;
    }

    //Getter Id
    get Id() {
        return this.Id;
    }

    //Setter Id
    set Id(id){
        this.Id(id);
    }

    //Getter Game
    get Game(){
        return this.Game;
    }

    //Setter Game
    set Game(game){
        this.Game(game);
    }

    //Getter Player
    get Player(){
        return this.Player;
    }

    //Setter Player
    set Player(player){
        this.Player(player);
    }

}