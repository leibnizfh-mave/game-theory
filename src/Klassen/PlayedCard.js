class PlayedCard {
    constructor(Game, Player, Round, PlayedCards) {
        this.Game = Game;
        this.Player = Player;
        this.Round = Round;
        this.PlayedCards = PlayedCards;
    }
        //Getter Game
        get Game(){
            return this.Game;
        }
        //Setter Game
        set Game(game){
            this.Game(game);
        }

        //Getter Player
        get Player(){
            return this.Player;
        }
        //Setter Player
        set Player(player){
            this.Player(player);
        }

        //Getter Round
        get Round(){
            return this.Round;
        }
        //Setter Round
        set Round(round){
            this.Round(round);
        }

        //Getter PlayedCards
        get PlayedCards(){
            return this.PlayedCards;
        }
        //Setter PlayedCards
        set PlayedCards(playedcards){
            this.PlayedCards(playedcards);
        }



}