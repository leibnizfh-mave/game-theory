
class Professor{
    //Constructor der Professor Klasse
    constructor(Id, FirstName, LastName, Password) {
        this.Id = Id;
        this.FirstName = FirstName;
        this.LastName = LastName;
        this.Password = Password;
    }

    //Getter Id
    get Id(){
        return this.Id;
    }
    //Setter Id
    set Id(id){
        this.Id(id);
    }

    //Getter FirstName
    get FirstName(){
        return this.FirstName;
    }
    //Setter FirstName
    set FirstName(first_name){
        this.FirstName(first_name);
    }

    //Getter LastName
    get LastName(){
        return this.LastName;
    }
    //Setter LastName
    set LastName(lastname){
        this.LastName(lastname);
    }

    //Getter Password
    get Password(){
        return this.Password;
    }
    //Setter Password
    set Password(password){
        this.Password(password);
    }
}


