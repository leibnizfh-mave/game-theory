
class Card{
    //Constructor der Card Klasse
    constructor(Id, CardName, Value) {
        this.Id = Id;
        this.CardName = CardName;
        this.Value = Value;
    }

    //Getter Id
    get Id(){
        return this.Id;
    }

    //Setter Id
    set Id(id){
        this.Id(id);
    }

    //Getter CardName
    get CardName(){
        return this.CardName;
    }

    //Setter CardName
    set CardName(card_name){
        this.CardName(card_name);
    }

    //Getter Value
    get Value(){
        return this.Value;
    }

    //Setter Value
    set Value(value){
        this.Value(value);
    }
}