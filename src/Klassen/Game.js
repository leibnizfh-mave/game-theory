class Game {
    constructor(Id, Creator, DateOfGame) {
        this.Id = Id;
        this.Creator = Creator;
        this.DateOfGame = DateOfGame;
    }

    get Id() {
        return this.Id;
    }

    set Id(id){
        this.Id(id);
    }

    get Creator(){
        return this.Creator;
    }

    set Creator(creator){
        this.Creator(creator);
    }

    get DateOfGame(){
        return this.DateOfGame;
    }

    set DateOfGame(dateofgame){
        this.DateOfGame(dateofgame);
    }
}