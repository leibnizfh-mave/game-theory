import React from 'react';
import './GameOverview.css';
import {Link} from "react-router-dom";


class GameOverview extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            var_gameName: ""
        }
    }

    render() {


            return (

                    <div class="game_overview">
                
                {/*Container mit unterschiedlichen Spieloptionen*/}
                <div className="side_container right"><h1 className="txt_game">PIT-Market Game</h1><button className="btn btn_gameSelect" >Spielstart</button></div>
                <div className="main_container"><h1 className="txt_game">Public Good Game</h1><Link to='/wartebereichdozent'><button  className="btn btn_gameSelect">Spielstart</button></Link></div>
                <div className="side_container left"><h1 className="txt_game">Lemon Market Game</h1><button className="btn btn_gameSelect">Spielstart</button></div>
            </div>
        );
    }

}

export default GameOverview;
