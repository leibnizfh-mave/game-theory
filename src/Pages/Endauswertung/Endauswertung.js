import React from 'react';
import {Link} from "react-router-dom";
import Popup from 'reactjs-popup';
import 'reactjs-popup/dist/index.css';

import DataCollection from "../Wartebereich/DataCollection";



class Endauswertung extends React.Component {
    constructor(props) {
        super(props);
        this.state = {


        };
    }

    render() {

        return (
            <div className="wartebereich_dozent">
                <div className="player_overview">
                    {/*Importiert Leiste links von Klasse DataCollection*/}
                    {DataCollection}

                    <button className="btn_down btn_hide"><i className="fa fa-chevron-down"></i></button>
                </div>

                <div className="main_content">
                    <span className="span_game">Gesamtauswertung</span>


                </div>
                <div className="bottomrow">
                    <button className="btn btn_nav">Gesamtauswertung</button>

                    <button className="btn btn_nav" > Export nach Excel</button>

                </div>

            </div>
        );

    }


}

export default Endauswertung;
