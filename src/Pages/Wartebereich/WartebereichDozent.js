import React, {SyntheticEvent, useEffect} from 'react';
import './WartebereichDozent.css';
import DataCollection from "./DataCollection";
import {Link} from "react-router-dom";
import Popup from 'reactjs-popup';
import 'reactjs-popup/dist/index.css';
import Rundeneinstellungen from "./Rundeneinstellungen";
import SpielBeenden from "../DozentGame/SpielBeenden";
import {db} from '../../Klassen/FireBase/FireBaseConf';



class WartebereichDozent extends React.Component {
    randomIntFromInterval(min, max) { // min and max included
        return Math.floor(Math.random() * (max - min + 1) + min)
    }
    constructor(props) {




       // useEffect(()=>x=randomIntFromInterval(100000,999999))
        super(props);
        let x=this.randomIntFromInterval(100000,999999);
        db.collection("GameID").add({
        GameID: x
        })
        this.state = {
            var_gameName: "Public Good Game",
           // var_spielid: "12345", //--> 6 stellige game id --> onload random zahl + abgleich mit db
            var_spielid: x,
            var_spielerZahl: "23",
            showPopup: false,
            Data: 0


        };
        this.handleChange = this.handleChange.bind(this);
    }



    togglePopup() {
        this.setState({
            showPopup: !this.state.showPopup
        });

    }
    toggleEnd() {
        this.setState({
            showEnd: !this.state.showEnd
        });
    }



    render() {

        return (


            <div className="wartebereich_dozent">
                <div className="player_overview">


                    {DataCollection}
                    <button className="btn_down btn_hide"><i className="fa fa-chevron-down"></i></button>

                </div>

                <div className="main_content">
                    {/*übernimmt den Token des Spiels durch die variable var_gameName, muss durch Datenbankanbindung gefillt werden*/}
                    <span className="span_game">Sie haben das Spiel "{this.state.var_gameName}" gestartet:</span>
                    <span  className="span_gameID">{this.state.var_spielid}</span>
                    {this.state.showPopup ?
                        /*Schließt das Popup*/
                        <Rundeneinstellungen values={this.state.rundenanzahl} onChange={this.handleChange}
                                             text='Click "Close Button" to hide popup'

                                             closePopup={this.togglePopup.bind(this)}

                        />
                        : null
                    }
                    {this.state.showEnd ?
                        <SpielBeenden
                            text='Click "Close Button" to hide popup'
                            closePopup={this.toggleEnd.bind(this)}
                        />
                        : null
                    }

                </div>
                <div className="bottomrow">
                    <button className="btn btn_nav"onClick={this.toggleEnd.bind(this)}>Spielabbruch</button>
                    {/*Onclick öffnet sich das popup*/}
                    <button className="btn btn_nav" onClick={this.togglePopup.bind(this)}> Rundeneinstellungen</button>

                    <Link to='/DozentGame'><button className="btn btn_nav">Spielstart</button></Link>
                </div>

            </div>
        );

    }

    togglePopup() {
        this.setState({
            showPopup: !this.state.showPopup
        });
    }

    handleChange = event =>{
        console.log("Props", this.props);
        this.setState({
            rundenanzahl: event.target.value
        });
        console.log("Props", this.state.rundenanzahl);
    }
}

export default WartebereichDozent;
