import React from 'react';
import './WartebereichDozent.css';
import '../Endauswertung/Endauswertung.css';
import '../../Layout/MenueLeiste.css';

function DataCollection() {

    const dataCollection = [
        {
            id: 1,
            name: 'Alexis'
        }, {
            id: 2,
            name: 'Alina'
        },
        {
            id: 3,
            name: 'Alina'
        },
        ,
        {
            id: 3,
            name: 'Alina'
        },
        ,
        {
            id: 3,
            name: 'Alina'
        },
        {
            id: 3,
            name: 'Alina'
        },
        {
            id: 3,
            name: 'Alina'
        },
        {
            id: 3,
            name: 'Alina'
        },
        {
            id: 3,
            name: 'Alina'
        }
    ];
/*Variable anzahl*/
    var anzahl = dataCollection.length;
    return (
        <div className="div_dataCollection">
            <span className="span_spieler">
                {/*nimmt die Spieleranzahl aus der Variable anzahl, die der dataCollection.length entspricht.*/}
                Spieler: {anzahl}
                <div className="drop">

                    <div className="drop_kategorien">
                        {/*Filter zur auswahl der Sortierung der Ergebnisse*/}
                            <button className="btn_kategorien btn_hide"><i className="fa fa-caret-down"></i></button>
                            <div className="dropdown-content">

                                <label htmlFor="Aufsteigend">Aufsteigend:</label>
                                <input type="checkbox" name="Aufsteigend"></input>


                                <label htmlFor="Absteigend">Absteigend:</label>
                                <input type="checkbox" name="Absteigend"></input>
                                <hr></hr>
                                <button>Geld</button>
                                <button>Name</button>
                                <button>Schwarze Karte</button>
                                <button>Rote Karte</button>
                            </div>
                    </div>
                </div>
            </span>

            <ul>
                {/*Füllt die Zeilen links mit dem "name" der dataCollection*/}
                {
                    dataCollection.map((item) =>
                        <li key={item.id}>{item.name}</li>
                    )
                }
            </ul>

        </div>
    );


}

export default DataCollection();