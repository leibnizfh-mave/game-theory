import React, {useEffect, useState} from 'react';
import './WartebereichDozent.css';
import DataCollection from "./DataCollection";
import {Link} from "react-router-dom";
import Popup from 'reactjs-popup';
import 'reactjs-popup/dist/index.css';
import './Rundeneinstellungen.css';
import {db} from "../../Klassen/FireBase/FireBaseConf";


class Rundeneinstellungen extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            rundenanzahl: 0,
            rundendauer: 0,
            rot: 0,
            schwarz: 0

        }

        this.handleChange = this.handleChange.bind(this);


    }


    render() {

        var {values} = this.props;


        let rot;
        let schwarz;

        function setSchwarz() {
            /* Funktion zum Setzen des Kartenwertes durch den Prof */
            schwarz = document.getElementById("inputSchwarz").value
            db.collection("Card").doc("schwarze Karte").update({Value: "" + schwarz + ""})

        }

        function setRot() {
            rot = document.getElementById("inputRot").value
            db.collection("Card").doc("Rote Karte").update({Value: "" + rot + ""})
        }


        return (
            /*Popup mit Rundeneinstellungen*/

            <div className="popup">
                <div className="popup_inner">
                    <div className="field_login">
                        <div className="inp_labelRunde"><label>Rundenanzahl: </label></div>
                        {/*Sollen den Wert der Variablen oben erhalten, müssen noch durch Datenbankanbindung ermöglicht werden.*/}
                        <div><input type="text" className="inp_rundenData" values={this.state.rundenanzahl}
                                    onChange={this.handleChange}></input>
                        </div>
                    </div>
                    <div className="field_login">
                        <div className="inp_labelRunde"><label>Rundendauer in Sekunden: </label></div>
                        <div><input type="text" className="inp_rundenData" values={this.state.rundendauer}></input>
                        </div>
                    </div>
                    <div className="field_login">
                        <div className="inp_labelRunde"><label>Roter Kartenwert: </label></div>
                        <div><input type="text" id="inputRot" className="inp_rundenData"
                                    onChange={setRot} value={this.props.value}>
                        </input>
                        </div>
                    </div>
                    <div className="field_login">
                        <div className="inp_labelRunde"><label>Schwarzer Kartenwert: </label></div>
                        <div><input type="text" id="inputSchwarz" className="inp_rundenData"
                                    onChange={setSchwarz} value={this.props.value}>

                        </input>
                        </div>
                    </div>

                    {/*Schließt das Popup*/}
                    <button className="btn_save"
                            onClick={this.props.closePopup}>
                        <i className="fa fa-save"></i></button>

                </div>
            </div>
        );

    }


    handleChange = event => {
        console.log("Props", this.props);
        this.setState({
            rundenanzahl: event.target.value,


        });
        console.log("Props", this.state.rundenanzahl);

    }


}

export default Rundeneinstellungen;
