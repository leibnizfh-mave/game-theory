import React, {SyntheticEvent, useEffect} from 'react';
import './WartebereichStudent.css';
import DataCollection from "./DataCollection";
import {Link} from "react-router-dom";
import Popup from 'reactjs-popup';
import 'reactjs-popup/dist/index.css';
import Rundeneinstellungen from "./Rundeneinstellungen";
import SpielBeenden from "../DozentGame/SpielBeenden";





class WartebereichStudent extends React.Component {
    constructor(props) {


        super(props);
        this.state = {
            var_gameName: "Public Good Game",
            // var_spielid: "12345", //--> 6 stellige game id --> onload random zahl + abgleich mit db
            var_spielid: "Warten auf Spielstart",
            var_spielerZahl: "23",
            showPopup: false,
            Data: 0


        };
        this.handleChange = this.handleChange.bind(this);
    }


    togglePopup() {
        this.setState({
            showPopup: !this.state.showPopup
        });

    }

    toggleEnd() {
        this.setState({
            showEnd: !this.state.showEnd
        });
    }


    render() {

        return (


            <div className="wartebereich_student">
                <div className="player_overview">


                    {DataCollection}
                    <button className="btn_down btn_hide"><i className="fa fa-chevron-down"></i></button>

                </div>

                <div className="main_content">
                    {/*übernimmt den Token des Spiels durch die variable var_gameName, muss durch Datenbankanbindung gefillt werden*/}
                    <span className="span_game"> "{this.state.var_gameName}" </span>
                    <span className="span_gameID">{this.state.var_spielid}</span>
                    {this.state.showPopup ?
                        /*Schließt das Popup*/
                        <Rundeneinstellungen values={this.state.rundenanzahl} onChange={this.handleChange}
                                             text='Click "Close Button" to hide popup'

                                             closePopup={this.togglePopup.bind(this)}

                        />
                        : null
                    }
                    {this.state.showEnd ?
                        <SpielBeenden
                            text='Click "Close Button" to hide popup'
                            closePopup={this.toggleEnd.bind(this)}
                        />
                        : null
                    }

                </div>


            </div>
        );

    }

    togglePopup() {
        this.setState({
            showPopup: !this.state.showPopup
        });
    }

    handleChange = event => {
        console.log("Props", this.props);
        this.setState({
            rundenanzahl: event.target.value
        });
        console.log("Props", this.state.rundenanzahl);
    }
}



export default WartebereichStudent;