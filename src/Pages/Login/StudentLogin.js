import React, {useEffect, useState} from 'react';
import './DozentLogin.css';
import {Link} from "react-router-dom";
import {db} from '../../Klassen/FireBase/FireBaseConf';

var postData = [];

const StudentLogin = () => {

    const [userName, setUserName] = useState("");
    const [gameID, setGameID] = useState("");


    const handleSubmit = (e) => {
        e.preventDefault();

        /*Wenn eines der Werte leer ist, kommt eine Fehlermeldung*/
        if (userName == "" || gameID == "") {

            alert("Bitte einen Wert eingeben!");

        }
        let i = 0;
        console.log(postData);
        while (i <= postData.length) {
            if (gameID == postData[i].GameID) {
                window.location.replace("http://localhost:3000/WartebereichStudent");


                break;
            } else {
                i++;
            }

            {
                /*Daten werden in die datenbank geschrieben*/
                db.collection("Player").add({
                    UserName: userName,
                    GameID: gameID,

                })

                    .then(() => {
                        alert("Message has been submitted!");
                    })
                    .catch((error) => {
                        alert(error.message);
                    })


                setUserName("");
                setGameID("");
            }


        }
    }
    const [posts, setPosts] = useState();

    useEffect(() => {  //Userdaten aus Db abholen und in Array speichern

        return db.collection('GameID').onSnapshot((snapshot) => {

            snapshot.forEach((doc) => postData.push({...doc.data(), id: doc.id}));
            setPosts(postData);
        });
    }, []);

    return (
        <div class="dozentLogin">
            <h1 className="logo_gametheory">Game Theory</h1>
            <div className="loginContainer">
                <label className="lbl_view">Student </label>
                <div className="field_login">

                    <div className="inp_label"><label>Benutzername: </label></div>
                    <div>
                        {/*onChange wird die Variable (z.b. userName) auf den aktuellen Wert gesetzt.*/}
                        <input
                            type="text"
                            className="inp_loginData"
                            value={userName}
                            onChange={(e) => setUserName(e.target.value)}
                        >
                        </input>
                    </div>
                </div>
                <div className="field_login">
                    <div className="inp_label"><label>Spiel-ID: </label></div>
                    <div>
                        <input
                            type="text"
                            className="inp_loginData"
                            value={gameID}
                            onChange={(e) => setGameID(e.target.value)}
                        >
                        </input>
                    </div>
                </div>

                <div>
                    {/*beim klicken wird die methode handlesubmit ausgeführt.*/}
                    <button onClick={handleSubmit} className="inp_loginData btn btn_login">Login</button>
                </div>
                {/*link zum dozentLogin*/}
                <Link to='/dozentlogin'><span className="span_switch">Dozent Login<button className="btn_hide"><i
                    className="fa fa-chevron-right"></i></button></span></Link>
            </div>
        </div>


    );


}
export default StudentLogin;
