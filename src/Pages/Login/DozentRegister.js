import React, {useState} from 'react';
import './DozentLogin.css';
import {Link} from "react-router-dom";
import {db} from '../../Klassen/FireBase/FireBaseConf';

const DozentRegister = () => {

    const [profName, setprofName] = useState("");
    const [email, setEmail] = useState("");
    const [profPasswort, setprofPasswort] = useState("");
    const [passwortBestaetigen, setPasswortBestaetigen] = useState("");
    const [passwordShown, setPasswordShown] = useState(false);
    const togglePasswordVisiblity = () => {
        setPasswordShown(passwordShown ? false : true);
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        /*Wenn nicht alle Werte ausgefüllt sind kommt eine Fehlermeldung.*/
        if (profName == "" || email == "" || profPasswort == "" || passwortBestaetigen == "") {
            console.log("if")
            alert("Bitte füllen Sie alle Werte aus.");
        } else {
            /*Wenn Passwörter nicht übereinstimmen kommt eine Fehlermeldung.*/
            if (profPasswort != passwortBestaetigen) {
                alert("Passwörter stimmen nicht überein.");
            } else {

                console.log("pw", profPasswort);
                /*Schreibt die eingegebenen Daten in die Datenbank.*/
                db.collection("Professor").add({
                    ProfName: profName,
                    Email: email,
                    Passwort: profPasswort,
                    PasswortBestaetigen: passwortBestaetigen
                })
                    .then(() => {
                        alert("Message has been submitted!");

                    })
                    .catch((error) => {
                        alert(error.message);
                    })
                setprofName("");
                setEmail("");
                setprofPasswort("");
                setPasswortBestaetigen("");
            }
        }
    }

    return (


        <div class="dozentLogin">

            <h1 className="logo_gametheory">Game Theory</h1>

            <div className="loginContainer">

                <label className="lbl_view">Dozent </label>
                <div className="field_login">

                    <div className="inp_label"><label>Name: </label></div>
                    <div>
                        {/*onChange werden die Variablen auf den jeweiligen Wert der Eingabe gesetzt.*/}
                        <input
                            type="text"
                            className="inp_loginData"
                            value={profName}
                            onChange={(e) => setprofName(e.target.value)}
                        >
                        </input>
                    </div>
                </div>
                <div className="field_login">

                    <div className="inp_label"><label>Email-Adresse: </label></div>
                    <div>
                        <input
                            type="text"
                            className="inp_loginData"
                            value={email}
                            onChange={(e) => setEmail(e.target.value)}
                        >
                        </input>
                    </div>
                </div>
                <div className="field_login">

                    <div className="inp_label"><label>Passwort: </label></div>
                    <div>
                        <input
                            className="inp_loginData"
                            type={passwordShown ? "text" : "password"}
                            value={profPasswort}
                            onChange={(e) => setprofPasswort(e.target.value)}>
                        </input>
                        <span className="span_hide">
                            {/*onclick wird das Passwort ein oder ausgeblendet*/}
                            <button className="btn_hide" onClick={togglePasswordVisiblity}>
                                <i className="fa fa-eye"></i>
                            </button>
                        </span>
                    </div>
                </div>
                <div className="field_login">
                    <div className="inp_label"><label>Passwort bestätigen: </label></div>
                    <div>
                        <input
                            className="inp_loginData"
                            type={passwordShown ? "text" : "password"}
                            value={passwortBestaetigen}
                            onChange={(e) => setPasswortBestaetigen(e.target.value)}>
                        </input>
                        <span className="span_hide">
                            <button className="btn_hide" onClick={togglePasswordVisiblity}>
                                <i className="fa fa-eye"></i>
                            </button>
                        </span>
                    </div>
                </div>
                <form>
                    {/*onclick werden die Daten abgesendet*/}
                    <button onClick={handleSubmit} className="inp_loginData btn btn_login"
                            type={"submit"}>Registrieren
                    </button>
                </form>
                <div className="div_password">
                    <Link to='/dozentlogin'><a href="#" className="href_password">Zurück zum Login</a></Link>
                </div>

            </div>
        </div>
    );
}
export default DozentRegister;
