import React, {useState, useEffect, SyntheticEvent} from 'react';
import './DozentLogin.css';
import {Link} from "react-router-dom";
import {db} from "../../Klassen/FireBase/FireBaseConf";

var postData = [];


const DozentLogin = () => {

    const [email, getEmail] = useState("");
    const [profPasswort, getprofPasswort] = useState("");
    const [dataArray, getData] = useState("");
    const [passwordShown, getPasswordShown] = useState(false);
    const togglePasswordVisiblity = () => {
        getPasswordShown(passwordShown ? false : true);
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        /*Wenn beide Felder nicht ausgefüllt sind kommt eine Fehlermeldung.*/
        let i = 0;
        if (email == "" || profPasswort == "") {
            console.log("if")
            alert("Ungültige Anmeldung.");
        } else {


            getprofPasswort("");
            getEmail("");

//Login Vergleich der Anmeldedaten + Weiterleitung
            while (i <= postData.length) {
                if (email == postData[i].Email && profPasswort == postData[i].Passwort) {
                    window.location.replace("http://localhost:3000/overview");
                    break;
                } else {
                    i++;
                }


            }


        }
    }

    const [posts, setPosts] = useState();

    useEffect(() => {  //Userdaten aus Db abholen und in Array speichern

        return db.collection('Professor').onSnapshot((snapshot) => {

            snapshot.forEach((doc) => postData.push({...doc.data(), id: doc.id}));
            setPosts(postData);
        });
    }, []);


    return (
        <div class="dozentLogin">
            <h1 className="logo_gametheory">Game Theory</h1>
            <div className="loginContainer">
                <label className="lbl_view">Dozent </label>
                <div className="field_login">

                    <div className="inp_label"><label>Email-Adresse: </label></div>
                    {/*onChange = wenn Inhalt des Feldes geändert wird, wird die Funktion getEmail ausgeführt und die Variable Email auf den Wert gesetzt.*/}
                    <div><input
                        type="text"
                        className="inp_loginData"
                        value={email}
                        onChange={(e) => getEmail(e.target.value)}
                    >
                    </input>
                    </div>
                </div>
                <div className="field_login">
                    <div className="inp_label"><label>Passwort: </label></div>
                    <div>
                        <input
                            className="inp_loginData"
                            type={passwordShown ? "text" : "password"}
                            value={profPasswort}
                            onChange={(e) => getprofPasswort(e.target.value)}>
                        </input>
                        <span className="span_hide">
                                {/*onClick = Wenn geclickt wird die Methode togglePasswordVisibility (siehe oben) ausgeführt, Passwort wird gezeigt oder versteckt.*/}
                            <button className="btn_hide" onClick={togglePasswordVisiblity}>
                                <i className="fa fa-eye"></i>
                            </button>
                        </span>
                    </div>
                </div>

                <div>
                    {/*onClick = Wenn Button geclickt wird, wird die methode handleSubmit ausgeführt und die Daten werden abgesendet.*/}
                    <button className="inp_loginData btn btn_login" onClick={handleSubmit}>Login</button>

                </div>
                <div className="div_password">
                    <a href="#" className="href_password">Passwort vergessen?</a>
                </div>
                <div className="div_register">
                    <Link to='/dozentregister'><a href="#" className="href_password">Registrieren</a></Link>
                </div>
                {/*Verlinkung zum Studentenlogin*/}
                <Link to='/studentlogin'><span className="span_switch">Studenten Login<button
                    className="btn_hide"><i className="fa fa-chevron-right"></i></button></span></Link>
            </div>
        </div>


    );

}

export default DozentLogin;
