import React from 'react';
import './Kontoeinstellung.css';


class Kontoeinstellung extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            var_gameName: ""
        }
    }
    render() {
        return (
            /*Kontoeinstellungsoptionen.*/
            <div class="kontoeinstellung">
                <div className="container-left">
                    <div className="label_konto"> <label>E-Mail ändern</label></div>
                    <div className="textfield_Email"><input className="konto_textarea" type="textarea"></input></div>
                    <div className="label_konto"><label>Passwort ändern</label></div>
                    <div className="textfield_Passwort_aendern"><input className="konto_textarea" type="password"></input></div>
                    <div className="label_konto"><label className="label-text">Passwort bestätigen</label></div>
                    <div className="textfield_Email_bestaetigen"><input className="konto_textarea" type="password"></input></div>
                    <div className="btn_save"><button className="btn btn_konto">Speichern</button> </div>
                </div>

            </div>
        );
    }

}

export default Kontoeinstellung;
