import React, {useEffect, useState} from 'react';
import './Kartenauswahl.css';
import {db} from "../../Klassen/FireBase/FireBaseConf";


class Kartenauswahl extends React.Component {


    constructor() {

        super();

        this.state = {
            disabled: false,
            valueCard: [],
            i: 0
        }

        this.handleInputChangeCard = this.handleInputChangeCard.bind(this);

    }


    /*Beim Clicken des Auswahl bestätigen Buttons */
    handleClick = (event) => {
        /*Button Auswahl bestätigen wird ausgegraut*/
        if (this.state.disabled) {
            return;
        }
        this.setState({disabled: true});
        // Send
        /*Es wird überprüft, welche Checkboxen gecheckt sind*/
        const cb1 = document.getElementById("selected-item-1");
        console.log(cb1.checked);
        const cb2 = document.getElementById("selected-item-2");
        console.log(cb2.checked);
        const cb3 = document.getElementById("selected-item-3");
        console.log(cb3.checked);
        const cb4 = document.getElementById("selected-item-4");
        console.log(cb4.checked);

    }

    /*Pro Karte eine Klasse --> Beim Clicken aufgerufen*/

    handleInputChangeCard(event) {

        const target = event.target;
        /*Wenn gecheckt erhöht sich der Zähler i um eins, dieser bestimmt die Anzahl der ausgewählten Checkboxen*/

        if (target.checked) {
            this.state.i++;
            console.log(this.state.i);
            /* Wenn mehr als 2 Checkboxen --> Fehlermeldung und Auswahl der Checkbox wird deaktiviert
                * Zähler i wird daraufhin um 1 reduziert*/
            if (this.state.i > 2) {
                alert("Bitte wählen Sie nur 2 aus.")
                target.checked = false;
                this.state.i--;
                console.log(this.state.i);
                /*Wenn nicht mehr als 2 wird die Box gechecked und der Wert in der Variable value1-4 gespeichert.*/
            } else {
                const target = event.target;
            }

        }
        /*Wenn die Checkbox entchecked wird --> Zähler i sinkt um 1 und der Value1-4 wird auf 0 Zurückgesetzt*/
        else {
            this.state.i--;
            console.log(this.state.i);
        }
    }

    async getMarkers() {
        const events = await db.collection('Card')
        events.get().then((querySnapshot) => {
            const tempDoc = []
            querySnapshot.forEach((doc) => {
                tempDoc.push({id: doc.id, ...doc.data()})
            })
            console.log(tempDoc)
        })

    }


    render() {
        let valueRot;
        let valueSchwarz
        let i = 0;

        function handleEvent() {
            db.collection('Card')
                .get()
                .then(snapshot => {
                    const cards = []
                    snapshot.forEach(doc => {
                        const data = doc.data()
                        cards.push(data)


                    })


                    while (i <= cards.length) {
                        if (cards[i].Farbe === 'rot') {
                            valueRot = cards[i].Value
                            console.log(valueRot)
                        }
                        if (cards[i].Farbe === 'schwarz') {
                            valueSchwarz = cards[i].Value
                            console.log(valueSchwarz)
                        }
                        i++
                    }
                })
                .catch(error => console.log(error))


        }

        function redCard() {
            document.getElementById('labelCard2').innerHTML = '' + valueRot + '€'
            document.getElementById('labelCard1').innerHTML = '' + valueRot + '€'
        }

        function blackCard() {
            document.getElementById('labelCard3').innerHTML = '' + valueSchwarz + '€'
            document.getElementById('labelCard4').innerHTML = '' + valueSchwarz + '€'
        }

        return (

            <div class="Kartenauswahl">
                <div onChange={handleEvent}>
                    <div onChange={redCard}>
                        <div onChange={blackCard}>
                            <div className="cardarea">


                                <div className="lbl-pick">Bitte wählen Sie zwei Karten aus.
                                </div>
                                <div className="lbl-round">Runde: 02/15</div>
                                {/*Karten 1 bis 4*/}
                                <div className="grid-wrapper grid-col-4">
                                    <div className="selection-wrapper">
                                        {/*Karte1*/}
                                        <label for="selected-item-1" className="selected-label">
                                            {/*Wenn Checkbox sich ändert wird Methode aufgerufen, siehe Oben*/}
                                            <input type="checkbox" name="selected-item" id="selected-item-1" value="1"
                                                   onChange={this.handleInputChangeCard}/>
                                            <span className="icon"></span>
                                            <div className="selected-content-left">
                                                <label className="lbl-card" id="labelCard1">valueRot </label>
                                            </div>

                                        </label>
                                    </div>
                                    <div className="selection-wrapper">
                                        {/*Karte2*/}
                                        <label for="selected-item-2" className="selected-label">
                                            {/*Wenn Checkbox sich ändert wird Methode aufgerufen, siehe Oben*/}
                                            <input type="checkbox" name="selected-item" id="selected-item-2" value="2"
                                                   onChange={this.handleInputChangeCard}/>
                                            <span className="icon"></span>
                                            <div className="selected-content-left">
                                                <label className="lbl-card" id="labelCard2">valueRot</label>
                                            </div>

                                        </label>
                                    </div>
                                    <div className="selection-wrapper">

                                        {/*Karte3*/}
                                        <label htmlFor="selected-item-3" className="selected-label">
                                            {/*Wenn Checkbox sich ändert wird Methode aufgerufen, siehe Oben*/}
                                            <input type="checkbox" name="selected-item" id="selected-item-3" value="3"
                                                   onChange={this.handleInputChangeCard}/>
                                            <span className="icon"></span>
                                            <div className="selected-content-right">
                                                <label className="lbl-card" id="labelCard3">valueSchwarz</label>
                                            </div>

                                        </label>
                                    </div>
                                    <div className="selection-wrapper">
                                        {/*Karte4*/}
                                        <label htmlFor="selected-item-4" className="selected-label">
                                            {/*Wenn Checkbox sich ändert wird Methode aufgerufen, siehe Oben*/}
                                            <input type="checkbox" name="selected-item" id="selected-item-4" value="4"
                                                   onChange={this.handleInputChangeCard}/>
                                            <span className="icon"></span>
                                            <div className="selected-content-right">
                                                <label className="lbl-card" id="labelCard4">valueScharz</label>
                                            </div>

                                        </label>
                                    </div>
                                </div>

                                <div className="btn_card">
                                    {/*Button, Wenn geclickt wird die Methode zum Button deaktivieren ausgeführt, siehe Ohen*/}
                                    <button className="btn btn_confirm" onClick={this.handleClick}
                                            disabled={this.state.disabled}> {this.state.disabled ? 'Warte auf Spieler...' : 'Auswahl bestätigen'}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        )
            ;
    }

}

export default Kartenauswahl;
