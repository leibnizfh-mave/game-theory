import React from 'react';
import './DozentGame.css';
import {Link} from "react-router-dom";
import Popup from 'reactjs-popup';
import 'reactjs-popup/dist/index.css';
import './NaechsteRunde.css';


class NaechsteRunde extends React.Component {
    constructor(props) {
        super(props);
        this.state = {


        }


    }

    render() {

        return (
            <div className="popup_next">
                <div className="popup_inner">
                        <div className="field_login">
                            <span className="span_sure">Sind Sie sicher, dass Sie die Runde wirklich beenden wollen?</span>
                        </div>
                    {/*Schließt Popup bei Knopfdruck*/}
                    <button className="btn_choice" onClick={this.props.closePopup}>Ja</button>
                    <button className="btn_choice" onClick={this.props.closePopup}>Nein</button>
                </div>
            </div>
        );

    }

}

export default NaechsteRunde;
