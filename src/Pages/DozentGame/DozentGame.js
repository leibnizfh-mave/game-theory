import React from 'react';
import './DozentGame.css';
import DataCollection from "../Wartebereich/DataCollection";
import {Link} from "react-router-dom";
import Popup from 'reactjs-popup';
import 'reactjs-popup/dist/index.css';
import NaechsteRunde from "./NaechsteRunde";
import Rundeneinstellungen from "../Wartebereich/Rundeneinstellungen";
import SpielBeenden from "./SpielBeenden";


class DozentGame extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            var_gameName: "Public Good Game",
            var_spielid: "1234",
            var_spielerZahl: "23",
            showPopup: false,
            showNext: false

        };
    }

    toggleNext() {
        this.setState({
            showNext: !this.state.showNext
        });
    }
        togglePopup() {
            this.setState({
                showPopup: !this.state.showPopup
            });

    }
    toggleEnd() {
        this.setState({
            showEnd: !this.state.showEnd
        });
    }

    render() {

        return (
            <div className="wartebereich_dozent">
                <div className="player_overview">
                    {/*Blendet die Leiste links mithilfe der Klasse DataCollection ein*/}
                    {DataCollection}
                    {/*Dropdown Button für das Filtermenü*/}
                    <button className="btn_down btn_hide"><i className="fa fa-chevron-down"></i></button>
                </div>
                <div className="main_content">
                    <span className="span_game">Rundenauswertung</span>
                    {/*Popup bei Rundeneinstellungen, öffnet über Klasse Rundeneinstellungen*/}
                    {this.state.showPopup ?
                        <Rundeneinstellungen
                            text='Click "Close Button" to hide popup'
                            closePopup={this.togglePopup.bind(this)}
                        />
                        : null
                    }
                    {/*Popup bei Nächste Runde, öffnet über Klasse Nächste Runde*/}
                    {this.state.showNext ?
                        <NaechsteRunde
                            text='Click "Close Button" to hide popup'
                            closePopup={this.toggleNext.bind(this)}
                        />
                        : null
                    }
                    {/*Popup bei Spielbeenden, öffnet über Klasse Spielbeenden*/}
                    {this.state.showEnd ?
                        <SpielBeenden
                            text='Click "Close Button" to hide popup'
                            closePopup={this.toggleEnd.bind(this)}
                        />
                        : null
                    }

                </div>
                <div className="bottomrow">
                    {/*Buttons*/}
                    <button className="btn btn_nav"onClick={this.toggleEnd.bind(this)}>Spielabbruch</button>
                    <button className="btn btn_nav" onClick={this.toggleNext.bind(this)}>Nächste Runde</button>
                    <button className="btn btn_nav" onClick={this.togglePopup.bind(this)}> Rundeneinstellungen</button>



                </div>

            </div>
        );

    }

    togglePopup() {
        this.setState({
            showPopup: !this.state.showPopup
        });
    }
}

export default DozentGame;
