import React from 'react';
import './anleitung.css';


class Instruction extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            var_gameName: ""
        }
    }
    render() {

        return (
            // Spieleanleitung
            <div class="Anleitung">
                <div className="instructionpage">
                    <div className="lbl-header">Anleitung: Public Good Game</div>
                    <p className="paragraph-Anleitung">Jeder von Ihnen bekommt vier Karten: zwei „rot“ (Herz/Karo) und zwei „schwarz“ (Pik/Kreuz). Alle Karten haben die gleiche Ziffer bzw. das gleiche Bild. <br></br>

                        Dieses Experiment beinhaltet 15 Runden, wobei Sie in jeder Spielrunde zwei Karten verdeckt spielen. Dies gilt ebenso für alle Studierende aus Ihrer Studiengruppe. Ihre Auszahlung hängt dabei von Ihren gespielten Karten, aber auch von den gespielten Karten Ihrer Mitspieler ab. Jeder spielt für sich.<br></br>

                        Zu Beginn jeder Runde komme ich (oder ein Helfer) zu Ihnen und Sie legen zwei der vier Karten verdeckt oben auf den Kartenstapel. Am Ende jeder Runde erhalten Sie die gespielten Karten verdeckt wieder zurück. Es wird nicht verkündet, wer welche Kartenfarbe gespielt hat.</p>
                    <div className="lbl-footer">Wir warten auf alle Spieler . . . </div>

                </div>
            </div>


        );
    }
}
export default Instruction;
