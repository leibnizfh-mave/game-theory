import React from "react";
import firebase from "firebase";

export const DemoInput = ({demo}) => {
    const [Nachname, setNachname] = React.useState(demo.Nachname);

    const onUpdate = () => {
        const db = firebase.firestore();
        db.collection('TestDB').doc(demo.id).set({...demo, Nachname});
    }

    const onDelete = () => {
        const db = firebase.firestore();
        db.collection('TestDB').doc(demo.id).delete();
    }

    return(
        <div>
            <input value={Nachname} onChange={(e) => {setNachname(e.target.value)}}/>
            <button onClick={onUpdate}>Update</button>
            <button onClick={onDelete}>Delete</button>
        </div>
    );
}
