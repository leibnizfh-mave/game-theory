import React, {useState, useEffect} from "react";
import {db} from '../../Klassen/FireBase/FireBaseConf';
import "./FireBaseDemoCSS.css";

const Demo = () => {
    const [vorname, setVorname] = useState("");
    const [nachname, setNachname] = useState("");
    const [passwort, setPasswort] = useState("");

    const handleSubmit = (e) => {
        e.preventDefault();

        db.collection("TestDB").add({
            Vorname: vorname,
            Nachname: nachname,
            Passwort: passwort,
        })
            .then(() => {
                alert("Message has been submitted!");
            })
            .catch((error) => {
                alert(error.message);
            })
        setVorname("");
        setNachname("");
        setPasswort("");
    }

    return(
        <form className={"form"} onSubmit={handleSubmit}>
            <h1>Professor</h1>

            <label>Vorname</label>
            <input
                placeholder={"Vorname"}
                value={vorname}
                onChange={(e) => setVorname(e.target.value)}
            />

            <label>Nachname</label>
            <input
                placeholder={"Nachname"}
                value={nachname}
                onChange={(e) => setNachname(e.target.value)}
            />

            <label>Passwort</label>
            <input
                placeholder={"Passwort"}
                value={passwort}
                onChange={(e) => setPasswort(e.target.value)}
            />

            <button type={"submit"}>Submit</button>
        </form>
    );
};

export default Demo;
