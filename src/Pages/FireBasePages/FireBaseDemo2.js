import React, {useState, useEffect} from "react";
import {db} from '../../Klassen/FireBase/FireBaseConf';
import "./FireBaseDemoCSS.css";
import {DemoInput} from "./DemoInput";

function App()
{
    const [demos, setDemos] = React.useState([]);

    React.useEffect(() => {
        const fetchData = async () => {
            const data = await db.collection("TestDB").get();
            setDemos(data.docs.map(doc => ({...doc.data(), id: doc.id})));
        }
        fetchData().then(r => "");
    }, [])

    return(
        <div>
            {demos.map(demo => (
                <h1 key={demo.Nachname}>
                    <DemoInput demo={demo}/>
                </h1>
            ))}
        </div>
    );
}

export default App;
