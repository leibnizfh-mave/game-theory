import React from 'react';
import './App.css';
import DozentMenueLeiste from "./Layout/DozentMenueLeiste";
import StudentMenueLeiste from "./Layout/StudentMenueLeiste";
import Anleitung from "./Pages/Anleitung/Anleitung";
import Kartenauswahl from "./Pages/Kartenauswahl/Kartenauswahl";
import GameOverview from "./Pages/GameOverview/GameOverview";
/*Ermöglicht den Router, um durch die Seiten klicken zu können*/
import {BrowserRouter as Router, Switch, Route, Redirect} from 'react-router-dom';
import Layout from "./Layout/Layout";
import DozentLogin from "./Pages/Login/DozentLogin";
import DozentRegister from "./Pages/Login/DozentRegister";
import StudentLogin from "./Pages/Login/StudentLogin";
import Kontoeinstellung from "./Pages/Kontoeinstellung/Kontoeinstellung";
import WartebereichDozent from "./Pages/Wartebereich/WartebereichDozent";
import Rundeneinstellungen from "./Pages/Wartebereich/Rundeneinstellungen";
import DozentGame from "./Pages/DozentGame/DozentGame";
import NaechsteRunde from "./Pages/DozentGame/NaechsteRunde";
import Endauswertung from "./Pages/Endauswertung/Endauswertung";
import FireBaseDemo from './Pages/FireBasePages/FireBaseDemo';
import FireBaseDemo2 from './Pages/FireBasePages/FireBaseDemo2';
import WartebereichStudent from "./Pages/Wartebereich/WartebereichStudent";


function App() {
    return (
        /*Beginn des Routers, hierin werden alle Seiten den URLS zugeteilt*/
        <Router>
            <div className="App">
                <div className="main">
                    <Switch>
                        {/*Format:
                <Route path ="/URL" exact component={PageName}/>
                PageName = Name der .js Datei*/}
                        <Route path ="/" exact>
                            <Redirect to="/dozentlogin" />
                        </Route>
                        <Route path ="/dozentlogin" exact component={DozentLogin}/>
                        <Route path ="/dozentregister" exact component={DozentRegister}/>
                        <Route path ="/studentlogin" exact component={StudentLogin}/>
                        <Route path ="/rundeneinstellungen" exact component={Rundeneinstellungen}/>
                        <Route path ="/naechsteRunde" exact component={NaechsteRunde}/>
                        <Route path="/FireBaseDemo"exact component={FireBaseDemo}/>
                        <Route path="/FireBaseDemo2"exact component={FireBaseDemo2}/>
                        <Layout>
                            <Route path ="/overview" exact component={GameOverview}/>
                            <Route path ="/dozentgame" exact component={DozentGame}/>
                            <Route path ="/endauswertung" exact component={Endauswertung}/>
                            <Route path ="/dozent" exact component={DozentMenueLeiste}/>
                            <Route path ="/student" exact component={StudentMenueLeiste}/>
                            <Route path ="/Anleitung" exact component={Anleitung}/>
                            <Route path ="/wartebereichdozent" exact component={WartebereichDozent}/>
                            <Route path ="/wartebereichstudent" exact component={WartebereichStudent}/>
                            <Route path ="/Kartenauswahl" exact component={Kartenauswahl}/>
                            <Route path ="/Kontoeinstellung"exact component={Kontoeinstellung}/>
                        </Layout>
                    </Switch>
                </div>
            </div>
        </Router>
    );
}

export default App;
